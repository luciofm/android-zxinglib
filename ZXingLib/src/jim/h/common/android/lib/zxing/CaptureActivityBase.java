package jim.h.common.android.lib.zxing;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;

/**
 * Created by luciofm on 7/12/13.
 */
public class CaptureActivityBase extends Activity {
    public ViewfinderView getViewfinderView() {
        return null;
    }

    public Handler getHandler() {
        return null;
    }

    public void handleDecode(Result rawResult, Bitmap barcode) {

    }

    public void drawViewfinder() {

    }
}
